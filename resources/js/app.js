// Базовые зависимости
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
// Импортируем представления для VueRouter
/** Главный компонент приложения */
import App from './views/App';
/** URL /test */
import Test from './views/Test';
/** URL / */
import Home from './views/Home';

// Подключаем модуль VueRouter к Vue конструктору
Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        pageName: null,
        pageTip: null
    },
    getters: {
        pageName: state => {
            return state.pageName;
        },

        pageTip: state => {
            return state.pageTip;
        }
    },
    mutations: {
        setPageName: (state, payload) => {
            state.pageName = payload;
        },

        setPageTip: (state, payload) => {
            state.pageTip = payload;
        }
    },
    actions: {
        changePageName: (injectee, payload) => {
            injectee.commit('setPageName', payload);
        },

        changePageTip: (injectee, payload) => {
            injectee.commit('setPageTip', payload);
        }
    },
});

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/test',
            name: 'test',
            component: Test,
        },
        { path: '/api' },
        { path: '*', redirect: '/' }
    ]
});

Vue.prototype.$vueVersion = Vue.version;
Vue.prototype.$vueRouterVersion = VueRouter.version;

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    store,
    router
});

