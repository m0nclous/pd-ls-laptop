{{-- Используем стандарт HTML5 --}}
    <!doctype html>
<html lang="ru">
<head>
    {{-- Кодировка страницы --}}
    <meta charset="UTF-8">

    {{-- Мета теги для адаптива на телефонах --}}
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- Заголовок страницы --}}
    <title>PD-LS: Laptop</title>

    {{-- Описание страницы --}}
    <meta name="Description" content="Полицеский компьютер PD-LS.">

    {{-- Предзагрузка файлов --}}
    <link rel="preload" href="{{ mix('css/app.css') }}" as="style">
    <link rel="preload" href="{{ mix('js/app.js') }}" as="script">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
{{-- Главный Vue компонент --}}
<div id="app">
    <app></app>
</div>

{{-- Подключаем js файл, созданный Webpack --}}
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
