<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Задаем маршрут который отвечает на любой запрошенный URL и передает его в SpaController.
 * Без этого маршрута пользователь будет получать 404 ошибку при попытке зайти по адресу /test например.
 */
Route::get('/{any}', 'SpaController@index')->where('any', '(?!api|fonts)(.*)');
